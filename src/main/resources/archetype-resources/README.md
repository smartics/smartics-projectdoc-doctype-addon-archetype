${projectName}
============================

##Overview

This is a free add-on for [projectdoc](https://www.smartics.eu/confluence/display/PDAC1/) for [Confluence](https://www.atlassian.com/software/confluence).

The add-on provides blueprints to create pages for

  * X
  * X Type

It also provides space blueprints to get started with your documentation project quickly.

##Fork me!
Feel free to fork this project to adjust the templates according to your project requirements.

The add-on is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

##Documentation

For more information please visit

  * the add-on's homepage
  * the add-on on the Atlassian Marketplace