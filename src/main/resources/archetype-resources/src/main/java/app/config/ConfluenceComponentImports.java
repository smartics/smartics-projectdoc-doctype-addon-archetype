#set($symbol_pound='#')#set($symbol_dollar='$')#set($symbol_escape='\') package ${package}.app.config;

import com.atlassian.confluence.plugins.createcontent.ContentBlueprintManager;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.TemplateRendererHelper;
import com.atlassian.confluence.plugins.createcontent.api.services.ContentBlueprintService;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class ConfluenceComponentImports {
  @Bean
  public ApplicationProperties applicationProperties() {
    return importOsgiService(ApplicationProperties.class);
  }

  @Bean
  public I18nResolver i18nResolver() {
    return importOsgiService(I18nResolver.class);
  }

  @Bean
  public ContentBlueprintManager contentBlueprintManager() {
    return importOsgiService(ContentBlueprintManager.class);
  }

  @Bean
  public ContentBlueprintService contentBlueprintService() {
    return importOsgiService(ContentBlueprintService.class);
  }

  @Bean
  public TemplateRendererHelper templateRendererHelper() {
    return importOsgiService(TemplateRendererHelper.class);
  }

  @Bean
  public TemplateRenderer templateRenderer() {
    return importOsgiService(TemplateRenderer.class);
  }
}
