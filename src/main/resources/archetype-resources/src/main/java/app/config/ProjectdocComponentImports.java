#set($symbol_pound='#')#set($symbol_dollar='$')#set($symbol_escape=
    '\') package ${package}.app.config;

import de.smartics.projectdoc.atlassian.confluence.admin.space.SpaceAdminCenter;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.persistence.ProjectdocDocumentService;
import de.smartics.projectdoc.atlassian.confluence.persistence.ProjectdocMarkerSupport;
import de.smartics.projectdoc.atlassian.confluence.tools.i18n.I18nService;
import de.smartics.projectdoc.atlassian.confluence.tools.macro.CoreMacroServices;
import de.smartics.projectdoc.atlassian.confluence.util.ProjectdocUser;
import de.smartics.projectdoc.confluence.tool.blueprint.document.DocumentContextProviderSupportService;
import de.smartics.projectdoc.confluence.tool.blueprint.partition.PartitionContextProviderSupportService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class ProjectdocComponentImports {
  @Bean
  public ProjectdocDocumentService projectdocDocumentService() {
    return importOsgiService(ProjectdocDocumentService.class);
  }

  @Bean
  public ProjectdocUser projectdocUser() {
    return importOsgiService(ProjectdocUser.class);
  }

  @Bean
  public I18nService i18nService() {
    return importOsgiService(I18nService.class);
  }

  @Bean
  public SpaceAdminCenter spaceAdminCenter() {
    return importOsgiService(SpaceAdminCenter.class);
  }

  @Bean
  public ProjectdocMarkerSupport projectdocMarkerSupport() {
    return importOsgiService(ProjectdocMarkerSupport.class);
  }

  @Bean
  public ContextProviderSupportService contextProviderSupportService() {
    return importOsgiService(ContextProviderSupportService.class);
  }

  @Bean
  public PartitionContextProviderSupportService partitionContextProviderSupportService() {
    return importOsgiService(PartitionContextProviderSupportService.class);
  }

  @Bean
  public DocumentContextProviderSupportService documentContextProviderSupportService() {
    return importOsgiService(DocumentContextProviderSupportService.class);
  }

  @Bean
  public CoreMacroServices coreMacroServices() {
    return importOsgiService(CoreMacroServices.class);
  }
}
