#set($symbol_pound='#')#set($symbol_dollar='$')#set($symbol_escape='\')
package ${package}.app.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Bean;

import de.smartics.projectdoc.${shortId}.subspace.PartitionContextProvider;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;

@Configuration
@Import({ConfluenceComponentImports.class, ProjectdocComponentImports.class})
public class DefaultAppServicesConfig {
  @Bean
  public PartitionContextProvider partitionContextProvider(
      final ContextProviderSupportService support) {
    return new PartitionContextProvider(support);
  }
}