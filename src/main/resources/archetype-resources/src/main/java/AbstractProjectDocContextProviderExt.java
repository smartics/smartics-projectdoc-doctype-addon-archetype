#set($symbol_pound='#')#set($symbol_dollar='$')#set($symbol_escape='\')
    package ${package};

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.AbstractBlueprintContextProvider;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import de.smartics.projectdoc.confluence.tool.blueprint.document.DocumentContextProviderSupportService;

/**
 * Extends for injection.
 */
public abstract class AbstractProjectDocContextProviderExt
    extends AbstractBlueprintContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final DocumentContextProviderSupportService support;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  protected AbstractProjectDocContextProviderExt(
      final DocumentContextProviderSupportService support) {
    super(support.getTemplateRendererHelper());
    this.support = support;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    return support.updateBlueprintContext(blueprintContext);
  }

  // --- object basics --------------------------------------------------------

}
