#set($symbol_pound='#')#set($symbol_dollar='$')#set($symbol_escape='\')
package ${package};


import com.atlassian.confluence.plugins.createcontent.api.contextproviders.AbstractBlueprintContextProvider;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.TemplateRendererHelper;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocContextProvider;
import de.smartics.projectdoc.confluence.tool.blueprint.document.DocumentContextProviderSupportService;

/**
 * Extends for injection.
 */
public class ProjectDocContextProviderExt extends AbstractBlueprintContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final DocumentContextProviderSupportService support;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ProjectDocContextProviderExt(final DocumentContextProviderSupportService support) {
    super(support.getTemplateRendererHelper());
    this.support = support;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    return support.updateBlueprintContext(blueprintContext);
  }

  // --- object basics --------------------------------------------------------

}
