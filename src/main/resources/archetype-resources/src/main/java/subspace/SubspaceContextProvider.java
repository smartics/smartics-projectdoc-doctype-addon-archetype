#set($symbol_pound='#')#set($symbol_dollar='$')#set($symbol_escape=
    '\') package ${package}.subspace;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.AbstractBlueprintContextProvider;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import de.smartics.projectdoc.confluence.tool.blueprint.partition.BlueprintTemplateMatcher;
import de.smartics.projectdoc.confluence.tool.blueprint.partition.NoopNameAdjuster;
import de.smartics.projectdoc.confluence.tool.blueprint.partition.PartitionContextProviderSupportService;

/**
 * Extends for injection to a subspace.
 */
public class SubspaceContextProvider extends AbstractBlueprintContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String TEMPLATE_LABEL = "subspace-home";

  // --- members --------------------------------------------------------------

  private final PartitionContextProviderSupportService contextProviderService;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public SubspaceContextProvider(
      final PartitionContextProviderSupportService contextProviderService) {
    super(contextProviderService.getTemplateRendererHelper());
    this.contextProviderService = contextProviderService;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    return contextProviderService.updateBlueprintContext(blueprintContext,
        new BlueprintTemplateMatcher() {
          @Override
          public boolean matches(final String templateLabel) {
            return TEMPLATE_LABEL.equals(templateLabel);
          }
        }, new NoopNameAdjuster());
  }

  // --- object basics --------------------------------------------------------

}
