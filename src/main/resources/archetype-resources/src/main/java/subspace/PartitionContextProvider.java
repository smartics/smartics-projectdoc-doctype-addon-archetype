#set($symbol_pound='#')#set($symbol_dollar='$')#set($symbol_escape='\')
package ${package}.subspace;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocSpaceContextProvider;
import de.smartics.projectdoc.atlassian.confluence.page.partition.DoctypeBlueprintKeys;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class PartitionContextProvider extends ProjectDocSpaceContextProvider {

  public static final String DOCUMENT_MODULE_KEY =
      "${groupId}.${artifactId}:${package}.subspace-${shortId}-main-doctypehome-blueprint";

  /**
   * The names of templates of type doctypes.
   */
  private static final Set<String> TYPE_TEMPLATE_SHORT_KEYS;

  static {
    final Set<String> set = new HashSet<>();
    set.add("experience-level-home-template");
    set.add("module-type-home-template");
    set.add("resource-type-home-template");
    set.add("topic-type-home-template");

    // === projectdoc INSERT type-home-template ===

    TYPE_TEMPLATE_SHORT_KEYS = Collections.unmodifiableSet(set);
  }

  public PartitionContextProvider(final ContextProviderSupportService support) {
    super(support);
  }

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final BlueprintContext updatedBlueprintContext =
        super.updateBlueprintContext(blueprintContext);

    updatedBlueprintContext.put(DoctypeBlueprintKeys.DOCUMENTS_BLUEPRINT_KEY,
        DOCUMENT_MODULE_KEY);
    updatedBlueprintContext.put(
        DoctypeBlueprintKeys.ALTERNATIVE_HOMEPAGE_TEMPLATE_KEYS,
        TYPE_TEMPLATE_SHORT_KEYS);
    updatedBlueprintContext.put(
        DoctypeBlueprintKeys.ALTERNATIVE_HOMEPAGE_I18N_KEY,
        "projectdoc.doctype.type-home-template.name");

    return updatedBlueprintContext;
  }
}