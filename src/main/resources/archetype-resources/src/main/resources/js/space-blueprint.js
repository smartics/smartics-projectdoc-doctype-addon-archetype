
require(['ajs', 'confluence/root'], function (AJS, Confluence) {
    "use strict";

    AJS.bind("blueprint.wizard-register.ready", function () {
        function submitProjectdocSpace(e, state) {
            state.pageData.ContentPageTitle = state.pageData.name;
            return Confluence.SpaceBlueprint.CommonWizardBindings.submit(e, state);
        }
        function preRenderProjectdocSpace(e, state) {
            state.soyRenderContext['atlToken'] = AJS.Meta.get('atl-token');
            state.soyRenderContext['showSpacePermission'] = false;
        }

        /* === projectdoc INSERT register-space-wizard === */
    });
});